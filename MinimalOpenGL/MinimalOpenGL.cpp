// MinimalOpenGL.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "MinimalOpenGL.h"

// Globals
HINSTANCE hInst;
HGLRC g_hRC;


void display(RECT* prc, int dpi)
{
	// Some driver seem to need this:
	glViewport(0, 0, prc->right, prc->bottom);

	// While others need this:
//	glViewport(0, 0, (int)(prc->right * 96 / dpi), (int)(prc->bottom * 96 / dpi));

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, prc->right, prc->bottom, 0, -1, 1);

	RECT rcInset = *prc;
	InflateRect(&rcInset, -20 * dpi / 96, -20 * dpi / 96);

	glMatrixMode(GL_MODELVIEW);

	glClear(GL_COLOR_BUFFER_BIT);
	glBegin(GL_TRIANGLE_STRIP);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2i(rcInset.left, rcInset.top);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2i(rcInset.left, rcInset.bottom);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex2i(rcInset.right, rcInset.top);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2i(rcInset.right, rcInset.bottom);

	glEnd();
	glFlush();
}

bool InitOpenGLContext(HWND hWnd)
{
	// Setup pixel format
	HDC hDC = GetDC(hWnd);
	PIXELFORMATDESCRIPTOR pfd;
	memset(&pfd, 0, sizeof(pfd));
	pfd.nSize = sizeof(pfd);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	int pf = ChoosePixelFormat(hDC, &pfd);
	if (pf == 0 || !SetPixelFormat(hDC, pf, &pfd))
		return true;
	DescribePixelFormat(hDC, pf, sizeof(PIXELFORMATDESCRIPTOR), &pfd);
	g_hRC = wglCreateContext(hDC);
	ReleaseDC(hWnd, hDC);
	return false;
}

void CleanupOpenGLContext()
{
	wglDeleteContext(g_hRC);
}


void UpdateTitle(HWND hWnd)
{
	wchar_t sz[512];
	wsprintfW(sz, L"System: %i Window: %i", GetDpiForSystem(), GetDpiForWindow(hWnd));
	SetWindowText(hWnd, sz);
}


LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_PAINT:
		{
			PAINTSTRUCT ps;
			RECT rc;
			GetClientRect(hWnd, &rc);
			HDC hdc = BeginPaint(hWnd, &ps);
			wglMakeCurrent(hdc, g_hRC);
			display(&rc, GetDpiForWindow(hWnd));
			wglMakeCurrent(NULL, NULL);
			EndPaint(hWnd, &ps);
		}
		break;

		case WM_ERASEBKGND:
			return 0;

		case WM_DPICHANGED:
		{
			RECT* prc = (RECT*)lParam;
			SetWindowPos(hWnd, NULL, prc->left, prc->top, prc->right - prc->left, prc->bottom - prc->top, SWP_NOZORDER | SWP_NOACTIVATE);

			// Experiment - doesn't help
			CleanupOpenGLContext();
			InitOpenGLContext(hWnd);

			UpdateTitle(hWnd);
			break;
		}

		case WM_DESTROY:
			PostQuitMessage(0);
			break;
	}

	return DefWindowProc(hWnd, message, wParam, lParam);
}

int APIENTRY wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow)
{
	// Set Process DPI aware
	//SetProcessDPIAware();
	SetThreadDpiAwarenessContext(DPI_AWARENESS_CONTEXT_PER_MONITOR_AWARE_V2);

    // Register Class
	WNDCLASSEXW wcex;
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MINIMALOPENGL));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_MINIMALOPENGL);
	wcex.lpszClassName = L"MinimalOpenGL";
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
	RegisterClassExW(&wcex);

	// Create Window
	HWND hWnd = CreateWindowW(L"MinimalOpenGL", L"MinimalOpenGL", WS_OVERLAPPEDWINDOW,
		50, 50, 100, 100, nullptr, nullptr, hInstance, nullptr);
	if (!hWnd)
		return 7;

	SetWindowPos(hWnd, NULL, 0, 0, 500 * GetDpiForWindow(hWnd) / 96, 300 * GetDpiForWindow(hWnd) / 96, SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE);

	UpdateTitle(hWnd);

	InitOpenGLContext(hWnd);

	// Show window
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

    // Main message loop:
	MSG msg;
	while (GetMessage(&msg, nullptr, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

	CleanupOpenGLContext();

    return (int) msg.wParam;
}


