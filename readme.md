# Issues with OpenGL and Windows 10 in Monitor Aware DPI mode

This sample program demonstrates various issues with OpenGL and Windows 10's monitor aware DPI mode.

As seen in the images in the screen shots folder depending on the driver different scaling effects occur:

## Radeon RX460

This card works as expected at all DPI settings. Included as an example of how this is expected to work.

Here at standard 96dpi:

![RadeonRX460-1](screenshots/RadeonRX460/RadeonRX460-1.png)

Here at 144 dpi:

![RadeonRX460-3](screenshots/RadeonRX460/RadeonRX460-3.png)

## Radeon RX470

Same as RX460 - no issues.

## GTX660

Renders correctly when the window DPI is 96:

![gtx660-1](screenshots/gtx660/gtx660-1.png)

but any other window DPI display mis-scaled:

![gtx660-2](screenshots/gtx660/gtx660-2.png)

See screenshots folder for more examples.


## GTX760

Same as GTX660


## Intel Iris 5100 (Macbook Pro)

Renders correctly when the window DPI and system DPI match (all dpi's work)

![IntelIris5100](screenshots/IntelIris5100/IntelIris5100-8.png)

But when they mismatch (ie: after changing resolution but before sign-out/sign-in) the scaling is correct but shifted (most noticably vertically) with extra menu text appearing.  The offset appears to be the difference in the title bar height at the old dpi vs it's height at the new dpi.

![IntelIris5100](screenshots/IntelIris5100/IntelIris5100-7.png)

Again, see screen shots folder for more examples.

## Intel HD Graphics (Surface 3)

Same as Intel Iris 5100

## Steps to Reproduce

1. On Windows 10 Creators Edition
2. Run the MinimalOpenGL.exe (can be found in the root of this repository, or build from source)
3. Go to Display Settings
4. Change the Size of Text Apps and Other Items
5. View results.
6. Depending on card (eg: the intel ones), sign out and re-run the app.


## Links

Associated StackOverflow question can be found here:

https://stackoverflow.com/questions/47049651/opengl-and-windows-10-per-monitor-aware-dpi-scaling

Issue has been logged with Intel:

https://software.intel.com/en-us/forums/graphics-driver-bug-reporting/topic/747982

Issue has been logged with NVidia:  (No public link available)

	Q10:

	When running an OpenGL application that uses the new Window's 10 monitor aware DPI mode, ie:

	SetThreadDpiAwarenessContext(DPI_AWARENESS_CONTEXT_PER_MONITOR_AWARE_V2);

	the OpenGL rendering is incorrectly scaled.

	Example, program and screen shots available here:

	https://bitbucket.org/toptensoftware/minimalopengl/



	Q11:


	1. Download and build the example program from here: https://bitbucket.org/toptensoftware/minimalopengl/
	2. On Window's 10 Creators Edition set the screen scaling to anything other than 100% 
	3. Run the above mentioned program
	4. OpenGL rendering will be incorrectly scaled as shown in screen shots in linked page under GTX660.
	5. More screen shots in the screenshots sub-folder of the repository.

	Notes:

	* There's also a pre-built .exe in the mentioned bitbucket repo.

	* I've not been able to get clarification from Microsoft if this is correct behaviour but given that it's different to other drivers (eg: same software works perfectly on Radeon RX460, and almost correctly on Intel graphics), it appears this is probably an issue in the NVidia drivers.

	For more details on Window's 10 per-monitor DPI support, see these posts:

	* https://blogs.windows.com/buildingapps/2016/10/24/high-dpi-scaling-improvements-for-desktop-applications-and-mixed-mode-dpi-scaling-in-the-windows-10-anniversary-update/#Qzl29dhDjU6RahtA.97

	* https://blogs.windows.com/buildingapps/2017/04/04/high-dpi-scaling-improvements-desktop-applications-windows-10-creators-update/#wQ8fZuQII7hfvgs5.97



## Contact

Brad Robinson  
Topten Software  
contact@toptensoftware.com  

